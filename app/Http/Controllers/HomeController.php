<?php

namespace App\Http\Controllers;

use Faker\Provider\Base;
use App\Jobs\ResizeImage;
use GuzzleHttp\Middleware;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function newAnnouncement(Request $req) {
        $uniqueSecret = $req->old('uniqueSecret', base_convert(sha1(uniqid(mt_rand())), 16, 36));
        return view('announcements.new', compact('uniqueSecret'));
    }
    
    public function createAnnouncement(AnnouncementRequest $req) {
        $a = new Announcement();
        $a->title = $req->title;
        $a->body = $req->body;
        $a->category_id = $req->category;
        $a->user_id = Auth::user()->id;
        
        $a->save();
        
        $uniqueSecret = $req->input('uniqueSecret');
        
        $images = session()->get("images.{$uniqueSecret}",  []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);
        
        foreach ($images as $image) {
            $i = new AnnouncementImage();                                     // qui costruimamo l'announcement image, il blocco fino al save().
            
            $fileName = basename($image);
            $newFileName = "public/announcements/{$a->id}/{$fileName}";
            Storage::move($image, $newFileName);
            
            $i->file = $newFileName;
            $i->announcement_id = $a->id;
            
            $i->save();

                                                                       
            // GoogleVisionSafeSearchImage::withChain([     // qui lanciamo tutti i processing dei job asincroni in sequenza da lanciare
            //     new GoogleVisionLabelImage($i->id),     // prima c'era $newFileName, ma è lo stesso, è proprio quello memorizzato all'iterno del sistema
            //     new GoogleVisionRemoveFaces($i->id), 
            //     new ResizeImage($i->file, 300, 150),        // tutti i job con i parametri nei rispettivi costruttori.
            //     new ResizeImage($i->file, 400, 300)
            // ])->dispatch($i->id);

        }
        
        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
        
        return redirect(route('home'))->with('message', 'Annuncio inserito correttamente');
    }
    
    public function uploadImage(Request $req) {
        $uniqueSecret = $req->input('uniqueSecret');
        $fileName = $req->file('file')->store("public/temp/{$uniqueSecret}");

       dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));
        
        session()->push("images.{$uniqueSecret}", $fileName);
        
        return response()->json(
            // session()->get("images.{$uniqueSecret}")            qui passiamo tutte le immagini
            
            
            [
                'id' => $fileName,
            ]
            
            
        );  
    }

    public function removeImage(Request $req) {
           
        $uniqueSecret = $req->input('uniqueSecret');
        $fileName = $req->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);
        
        return response()->json('ok');

    }

    public function getImages(Request $req) {
        $uniqueSecret = $req->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach ($images as $image) {
            $data[] = [

                'id' => $image,
                // 'src'=>Storage::url($image) percorso dell'immagine originale
                'src' => AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }

        return response()->json($data);
  
    }
}

