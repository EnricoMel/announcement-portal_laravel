<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'=>'required|string|max:120',
            'body'=>'required|string|max:300',
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'È obbligatorio inserire un titolo',
            'title.max'=>'Il titolo non può contenere più di 120 caratteri',
            'body.required'=>'È obbligatorio inserire una descrizione',
            'body.max'=>"La descrizione non può superare i 300 caratteri",
        ];

    }
}
