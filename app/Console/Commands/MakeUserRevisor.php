<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class MakeUserRevisor extends Command
{
    
    protected $signature = 'presto:makeUserRevisor';              
    protected $description = 'Rendi un utente revisore';          
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function handle() {
        
        $email = $this->ask("Inserisci l'email dell'utente che vuoi rendere revisore");         // cattura l'email dalla console e la salviamo in $email
        $user = User::where('email', $email)->first();                                         // recuperiamo la prima occorrenza che ha quell'email, query al database e la salviamo in $user
         
        if(!$user) {                                                                           // se non viene trovato nel database usciamo con un errore
            $this->error('Utente non trovato');
            return;
        }

        $user->is_revisor = true;                                                          // se invece viene trovato, impostiamo il campo is_revisor su true, scriviamo nel database
        $user->save();
        $this->info("L'utente {$user->name} ora è un revisore");
        
    }
    
}
