<div class="col-9">
    <div class="card my-3">
        <div class="card-header">
            {{$announcement->title}}
        </div>
        <div class="card-body">
            <p>
                @foreach ($announcement->images as $image)   
                
                <img src= {{ $image->getUrl(300, 150)}} alt="" class="rounded float-end mx-1">

                @endforeach
                
                {{$announcement->body}}

               
            </p>
        </div>
        <div class="card-footer text-muted d-flex justify-content-between">
            <strong>Categoria: <a href="{{route('public.announcements.category', [$announcement->category->name, $announcement->category->id])}}">{{$announcement->category->name}}</a></strong>
            <i> {{$announcement->created_at->format('d/m/y')}} - {{$announcement->user->name}}</i>
        </div>
    </div>           
</div>