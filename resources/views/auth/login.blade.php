<x-layout>
    <x-slot name="title">Login</x-slot>
    
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <h3 class="text-center">Accedi</h3>
            </div>
        </div>
    </div>
    
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-12 col-md-4">
                <form method="POST" action="{{route('login')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail" class="form-label">Inserisci la tua email</label>
                        <input type="text" class="form-control" id="exampleInputEmail" name="email">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword" class="form-label">Inserisci la password</label>
                        <input type="password" class="form-control" id="exampleInputPassword" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary">Accedi</button>
                </form>
            </div>
        </div>
    </div>
    
</x-layout>