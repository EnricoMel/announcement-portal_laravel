<x-layout>
    <x-slot name="title">Registrati</x-slot>
    
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <h3 class="text-center">Registrati al portale</h3>
            </div>
        </div>
    </div>
    
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-12 col-md-4">
                <form method="POST" action="{{route('register')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputName" class="form-label">Inserisci il nome</label>
                        <input type="text" class="form-control" id="exampleInputName" name="name">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail" class="form-label">Inserisci la tua email</label>
                        <input type="email" class="form-control" id="exampleInputEmail" name="email">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword" class="form-label">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword" name="password">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPasswordConf" class="form-label">Conferma password</label>
                        <input type="password" class="form-control" id="exampleInputPasswordConf" name="password_confirmation">
                    </div>
                    <button type="submit" class="btn btn-primary">Registrati</button>
                </form>
            </div>
        </div>
    </div>
    
</x-layout>