<x-layout>
    <x-slot name="title">Presto.it</x-slot>
    
    <h3 class="text-center mt-3">{{ __('ui.welcome') }}</h3>

    @if (session('message'))
    <div class="alert alert-success text-center">
        {{ session('message') }}                {{-- Messaggio che sta nel redirect--}}
    </div>
    @endif

    @if (session('access.denied.revisor.only'))
    <div class="alert alert-danger text-center mt-4">
        Accesso non consentito - solo per revisori
    </div>
    @endif

    <div class="container mt-5">
        <div class="row justify-content-center">
            @foreach ($announcements as $announcement)
            @include('_announcement')
            @endforeach  
        </div>
    </div>
    
</x-layout>