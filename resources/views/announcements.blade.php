<x-layout>
    <x-slot name="title">{{$category->name}}</x-slot>
    
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-12">
                <h3 class="text-center">Annunci per categoria: {{$category->name}}</h3>
            </div>
        </div>
    </div>
    
    <div class="container mt-5">
        <div class="row justify-content-center">
            
            @foreach ($announcements as $announcement)
            @include('_announcement')
            @endforeach
            
        </div>   
    </div>
    
    <div class="container my-5">
        <div class="row justify-content-center text-center">
            <div class="col-12">
                {{$announcements->links()}}
            </div>
        </div>
    </div>
    
    
</x-layout>