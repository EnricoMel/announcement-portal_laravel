<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand fw-bolder" href="{{route('home')}}">Presto.it</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                
                @guest
                
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="{{route('register')}}">{{ __('ui.register')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">{{ __('ui.login')}}</a>
                </li>
                <li class="nav-item dropdown">
                    <a id="categoriesDropdown" class="nav-link dropdown-toggle fw-bolder" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Categorie <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="categoriesDropdown">
                        @foreach ($categories as $category)
                        <a class="nav-link" href="{{route('public.announcements.category', [$category->name, $category->id])}}">
                            {{$category->name}}
                        </a>
                        @endforeach
                    </div>
                    
                    @endguest
                    
                    {{-- Multilanguage section --}}
                    
                    <li class="nav-item">
                        @include('components\nav._locale', ['lang'=>'it', 'nation'=>'it'])
                    </li>
                    
                    <li class="nav-item">
                        @include('components\nav._locale', ['lang'=>'en', 'nation'=>'gb'])
                    </li>
                    
                    <li class="nav-item">
                        @include('components\nav._locale', ['lang'=>'es', 'nation'=>'es'])
                    </li>
                    
                    {{-- End multilanguage section --}}
                    
                    @auth
                    
                    @if (Auth::user()->is_revisor)
                    <li class="nav-item">
                        <a class="nav-link fw-bolder" href="{{route('revisor.home')}}">Revisor home
                            <span class="badge rounded-pill bg-warning text-dark">{{\App\Models\Announcement::ToBeRevsionedCount()}}</span>
                        </a>
                    </li>
                    @endif
                    
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle fw-bolder" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Benvenuto {{Auth::user()->name}}
                        </a>
                        
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">Logout</a></li>
                            <form method="POST" action="{{route('logout')}}" id="form-logout">
                                @csrf    
                            </form>
                            <li><a class="dropdown-item" href="{{route('announcement.new')}}">Nuovo annuncio</a></li>
                            {{-- <li><hr class="dropdown-divider"></li> --}}
                            {{-- <li><a class="dropdown-item" href="#">Something else here</a></li> --}}
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="categoriesDropdown" class="nav-link dropdown-toggle fw-bolder" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Categorie <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="categoriesDropdown">
                            @foreach ($categories as $category)
                            <a class="nav-link" href="{{route('public.announcements.category', [$category->name, $category->id])}}">
                                {{$category->name}}
                            </a>
                            @endforeach
                        </div>
                        @endauth
                        
                        {{-- <li class="nav-item">
                            <a class="nav-link disabled">Disabled</a>
                        </li> --}}
                    </ul>
                    {{-- <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit">Search</button>
                    </form> --}}
                </div>
            </div>
        </nav>