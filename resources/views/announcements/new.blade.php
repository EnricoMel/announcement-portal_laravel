<x-layout>
    <x-slot name="title">Nuovo annuncio</x-slot>
    
    <div class="container">
        <div class="row justify-content-center mt-5 text-center">
            <div class="col-12 col-md-6">
                <h1>Inserisci un nuovo annuncio</h1>
            </div>
        </div>
        
        @if ($errors->any())
        <div class="alert alert-warning">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        
        
        <div class="row justify-content-center mt-5">
            <div class="col-12 col-md-4">
                <h3>DEBUG:: SECRET {{ $uniqueSecret }}</h3>
                <form method="POST" action="{{route('announcement.create')}}">
                    @csrf
                    
                    <input 
                    type="hidden" 
                    name="uniqueSecret"
                    value="{{ $uniqueSecret }}">
                    
                    <div class="mb-3">
                        <select name="category" id="category">
                            @foreach ($categories as $category)
                            
                            <option value="{{$category->id}}" {{ old('category') == $category->id ? 'selected':''}}>{{$category->name}}</option>
                            
                            @endforeach
                        </select>                        
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputTitle" class="form-label">Inserisci il titolo</label>
                        <input type="text" class="form-control" id="exampleInputTitle" name="title" value="{{old('title')}}">
                    </div>
                    <div class="mb-3">
                        <textarea name="body" id="" cols="54" rows="10">{{old('body')}}</textarea>
                    </div>
                    
                    {{-- Insert images --}}
                    <div class="form-group row">
                        <label for="images" class="col-md-12 col-form-label text-md-right">Immagini</label>
                        <div class="col-md-12">
                            
                            <div class="dropzone" id="drophere"></div>

                        </div>
                    </div>
                    
                    
                    <button type="submit" class="btn btn-primary mt-5">Crea annuncio</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>