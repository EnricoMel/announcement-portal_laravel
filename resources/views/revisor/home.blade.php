<x-layout>
    <x-slot name="title">Console revisore</x-slot>
    
    @if($announcement)
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Annuncio #{{ $announcement->id }}
                    </div>
                    <div class="card-body">
                        
                        <div class="row">
                            <div class="col-md-2"><h3>Utente</h3></div>
                            <div class="col-md-10">
                                #{{ $announcement->user->id }},
                                {{ $announcement->user->name }},
                                {{ $announcement->user->email }},
                            </div>
                        </div>
                        
                        <hr>
                        
                        <div class="row">
                            <div class="col-md-2"><h3>Titolo</h3></div>
                            <div class="col-md-10">{{ $announcement->title }}</div>
                        </div>
                        
                        <hr>
                        
                        <div class="row">
                            <div class="col-md-2"><h3>Descrizione</h3></div>
                            <div class="col-md-10">{{ $announcement->body }}</div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2"><h3>Immagini</h3></div>
                            <div class="col-md-10">
                                @foreach ($announcement->images as $image)
                                    
                               
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <img src= {{ $image->getUrl(300, 150) }} class="rounded" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        {{-- Adult: {{ $image->adult }}
                                        Spoof: {{ $image->spoof }}
                                        Medical: {{ $image->medical }}
                                        Violence: {{ $image->violence }}
                                        Racy: {{ $image->racy }} --}}

                                        {{ $image->id }}
                                        {{ $image->file }}
                                        {{ Storage::url($image->file) }} <br> {{-- questa parte va tolta quando avvio l'api --}}

                                        {{-- <b>Labels</b><br>
                                        <ul>
                                            @if ($image->labels)
                                            @foreach ($image->labels as $label)
                                                <li>{{ $label }}
                                            @endforeach
                                            @endif
                                        </ul> --}}
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row justify-content-center mt-5">
            <div class="col-md-6 ">
                <form action="{{ route('revisor.reject', $announcement->id) }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-danger">Reject</button>
                </form>
            </div>
            
            <div class="col-md-6 text-end">
                <form action="{{ route('revisor.accept', $announcement->id) }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-success">Accept</button>
                </form>
            </div>
        </div>
    </div>
    
    @else
    
    <div class="alert alert-success text-center">
        Non ci sono annunci da revisionare
    </div>

    @endif
</x-layout>